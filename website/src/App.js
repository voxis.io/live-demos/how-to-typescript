import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './pages/Home';
import { APP_TITLE } from './constants';
import contents from './contents';

import 'normalize.css';
import './App.css';

export default function App() {

  document.title = APP_TITLE;

  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        {contents.map((category) => (
          category.pages.map((page, i) => (
            <Route key={i} exact path={page.path} component={page.component} />
          ))
        ))}
      </Switch>
    </Router>
  );
}
