import React from 'react';
import Banner from '../components/Banner';
import SearchBar from '../components/SearchBar';
import SectionList from '../components/SectionList';
import contents from '../contents';
import { VOXIS_LINK, APP_DESCRIPTION, APP_TITLE } from '../constants';

class Home extends React.Component {

  constructor(props) {
    super(props)
    this.state = { filterText: `` };
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
  };

  handleFilterTextChange(filterText) {
    this.setState({
      filterText,
    });
  }

  render() {
    return (
      <div>
        <Banner text={VOXIS_LINK} />
        <div className="container">
          <h1>{APP_TITLE}</h1>
          {APP_DESCRIPTION}
        </div>
        <SearchBar
          filterText={this.state.filterText}
          onFilterTextChange={this.handleFilterTextChange}
        />
        <SectionList
          contents={contents}
          filterText={this.state.filterText}
        />
      </div>
    );
  }

}

export default Home
