import React from 'react';
import Banner from '../components/Banner';
import { APP_TITLE, RETURN_HOME_LINK } from '../constants';

const FixXYZ = () => {
  document.title = `How to fix error x y z. | ${APP_TITLE}`;

  return (
    <div>
      <Banner text={RETURN_HOME_LINK} />
      <article className="container">
        <h1>How to fix error x y z.</h1>
        <p>Description</p>
      </article>
    </div>
  );
}

export default FixXYZ
