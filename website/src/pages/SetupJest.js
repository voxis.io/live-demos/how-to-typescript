import React from 'react';
import Banner from '../components/Banner';
import { APP_TITLE, RETURN_HOME_LINK } from '../constants';
import { PrismCode } from '../components/PrismCode';

const code = `
  <div>Example use of code</div>
`

const SetupJest = () => {
  document.title = `How to setup testing using Jest. | ${APP_TITLE}`;

  return (
    <div>
      <Banner text={RETURN_HOME_LINK} />
      <article className="container">
        <h1>How to setup testing using Jest.</h1>
        <p>Description</p>
        <PrismCode
          code={code}
          language="html"
          plugins={["line-numbers"]}
        />
      </article>
    </div>
  );
}

export default SetupJest;
