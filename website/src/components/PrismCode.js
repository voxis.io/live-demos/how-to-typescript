import React, { useEffect } from "react";
import Prism from "prismjs";

export const PrismCode = ({ code, plugins, language }) => {
  const inputRef = React.createRef();

  useEffect(() => {
    if (inputRef && inputRef.current) {
      Prism.highlightElement(inputRef.current);
    }
  });

  return (
    <pre className={!plugins ? "" : plugins.join(" ")}>
      <code ref={inputRef} className={`language-${language}`}>
        {code.trim()}
      </code>
    </pre>
  )
}