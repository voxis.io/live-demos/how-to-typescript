import React from 'react';
import { Link } from 'react-router-dom';

const ListItem = ({ name, path }) => {
  return (
    <li>
      <Link to={path}>
        {name}
      </Link>
    </li>
  )
};

export default ListItem;
