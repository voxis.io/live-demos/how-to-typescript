import React from 'react';
import { MDBInput } from "mdbreact";

class SearchBar extends React.Component {

  constructor(props) {
    super(props);
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
    this.handleEscKey = this.handleEscKey.bind(this);
    this.inputRef = React.createRef();
  }

  componentDidMount() {
    document.addEventListener(`keydown`, this.handleEscKey, false);
  }
  componentWillUnmount() {
    document.removeEventListener(`keydown`, this.handleEscKey, false);
  }

  handleFilterTextChange(e) {
    this.props.onFilterTextChange(e.target.value);
  }

  handleEscKey(e) {
    if (e.keyCode === 27) {
      this.props.onFilterTextChange(``);
    }
  }

  render() {
    return (
      <div className="container">
        <MDBInput
          label="How to..."
          ref={this.inputRef}
          autoFocus={(window.screen.width >= 400) ? true : false}
          value={this.props.filterText}
          onChange={this.handleFilterTextChange}
          background
        />
      </div>
    );
  }

}

export default SearchBar
