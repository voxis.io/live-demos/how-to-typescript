import React from 'react';
import { IS_STAGE_ENV } from '../constants';

const Banner = ({ text }) => (
  <>
    {IS_STAGE_ENV && <div className="test-env container">Staging Environment</div>}
    <div className="banner container">{text}</div>
  </>
);

export default Banner
