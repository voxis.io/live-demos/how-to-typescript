import React from 'react';
import ListItem from './ListItem';

const Category = ({ filterText, category }) => {

  const pages = [];

  if (category.categoryName.toLowerCase().includes(filterText)) {
    category.pages.forEach((page) => {
      pages.push(<ListItem key={page.name} name={page.name} path={page.path} />);
    });
  } else {
    category.pages.forEach((page) => {
      if (page.name.toLowerCase().includes(filterText)) {
        pages.push(<ListItem key={page.name} name={page.name} path={page.path} />);
      }
    });
  }

  return (
    <div className="container">
      <h2>{category.categoryName}</h2>
      <ul>
        {pages}
      </ul>
    </div>
  );
}

export default Category;
