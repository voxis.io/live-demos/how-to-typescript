import React from 'react';
import { Link } from 'react-router-dom';
import Category from './Category';

const SectionList = ({ filterText, contents }) => {

  const filterTextFormatted = filterText.toLowerCase().trim();
  const categories = [];

  contents.forEach((category) => {
    const found = category.pages.find(
      page => page.name.toLowerCase().includes(filterTextFormatted)
    );
    if (found || category.categoryName.toLowerCase().includes(filterTextFormatted)) {
      categories.push(
        <Category
          key={category.categoryName}
          category={category}
          filterText={filterTextFormatted}
        />
      );
    }
  });

  return (
    <div>
      {categories}
    </div>
  );


}

export default SectionList
