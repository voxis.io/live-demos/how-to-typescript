import React from 'react';
import SetupJest from './pages/SetupJest';
import FixXYZ from './pages/FixXYZ';

const contents = [{
  categoryName: "Beginners",
  pages: [{
    name: "How to setup testing using Jest",
    path: "/how-to-setup-testing-using-jest",
    component: SetupJest,
  },
  ]
}, {
  categoryName: "Testing",
  pages: [{
    name: "How to fix XYZ",
    path: "/how-to-fix-xyz",
    component: FixXYZ,
  },
  ]
}, {
  categoryName: "Common Errors",
  pages: [{
    name: "One Example",
    path: "/one-example",
    component: FixXYZ,
  },
  {
    name: "Another Example",
    path: "/another-example",
    component: FixXYZ,
  },
  ]
}];

export default contents;
