import React from 'react';
import { Link } from 'react-router-dom';

export const IS_STAGE_ENV = Boolean(location.host === 'stage.howtotypescript.dev');
export const VOXIS_LINK = <p>Sponsored by <a href="https://www.voxis.io/">Voxis &mdash; Code to Cloud in 60s</a></p>;
export const RETURN_HOME_LINK = <p><Link to="/">&larr; Return to howtotypescript.dev</Link></p>;
export const APP_TITLE = "How to TypeScript";
export const APP_DESCRIPTION = <p>howtotypescript.dev is an easy to use reference for the common questions that developers new to TypeScript usually ask. Enter a "how to" query in the search box or browse the list below.</p>;